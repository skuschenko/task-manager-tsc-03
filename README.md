# Task Manager

Console Application

# User Info

Name: Semyon Kuschenko

Email: skushchenko@tsconsulting

Company: tsc

# Software

* JDK 1.8

* OS Windows 

# Hardware

* RAM 16GB

* CPU i7

* HDD 128GB

# Run Program

```
java -jar ./task-manager.jar

```

# Screenshots
https://yadi.sk/d/1P5tffaZGLQ2hA?w=1